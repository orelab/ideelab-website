anim_grotte = function( dom )
{
	var mouseX, mouseY
	var moving = true
	var direction = true	// false = marche arriere
	var radius = 8			// 2.9
	
	
	var isUserInteracting = true,
	onMouseDownMouseX = 0, onMouseDownMouseY = 0,
	lon = 0, onMouseDownLon = 0,
	lat = 0, onMouseDownLat = 0,
	phi = 0, theta = 0;
	
	
	var scene = new THREE.Scene()
	scene.fog = new THREE.Fog( 0x000000, 0, 7 )
	
	var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 )
	camera.position.y = 0
	camera.position.x = 0
	camera.position.z = radius
	
	var renderer = new THREE.WebGLRenderer()
	renderer.setSize( $( dom ).width(), $( dom ).height() * 0.6 )
	renderer.setClearColor( 0xffffff, 1)
	
	
	var light1 = new THREE.AmbientLight( 0x404040 );
	//scene.add( light1 );
	
	var light2 = new THREE.DirectionalLight( 0xffffff )
	light2.position.set( 0, 0, 3 ).normalize()
	scene.add( light2 )
	
	var light3 = new THREE.DirectionalLight( 0xffffff )
	light3.position.set( 0, 0, -3 ).normalize()
	scene.add( light3 )
	
	$( dom ).html( renderer.domElement )
	
	$( dom ).on({
		'mousewheel':function(e){
			e.preventDefault()
			camera.fov += e.deltaY*3
			if(camera.fov>100) camera.fov = 100
			if(camera.fov<40) camera.fov = 40
			camera.updateProjectionMatrix()
		},
		'mousedown':function(e){
			isUserInteracting = true
			onPointerDownPointerX = e.clientX
			onPointerDownPointerY = e.clientY
			onPointerDownLon = lon
			onPointerDownLat = lat
		},
		'mouseup':function(e){
			isUserInteracting = false;
		},
		'mousemove':function( e ){
			if ( isUserInteracting === true ){
				lon = ( onPointerDownPointerX - e.clientX ) * 0.1 + onPointerDownLon;
				lat = ( e.clientY - onPointerDownPointerY ) * 0.1 + onPointerDownLat;
			}
		},
	})
	
	$( dom ).on('resize',function(){
	    camera.aspect = window.innerWidth / window.innerHeight
	    camera.updateProjectionMatrix()
	    renderer.setSize( window.innerWidth, window.innerHeight )
	    render()
	})
	
	var loader = new THREE.JSONLoader()
	
	loader.load( 'assets/3d/grotte-smooth.js', function( geometry ){
		
		material = new THREE.MeshPhongMaterial( {
			color:0xE7906A,
			map: THREE.ImageUtils.loadTexture('assets/3d/textures.png'),
			side: THREE.DoubleSide
		})
	
		textures = [
			[ new THREE.Vector2(0, 6/7), new THREE.Vector2(1, 6/7), new THREE.Vector2(1,   1), new THREE.Vector2(0,   1) ],
			[ new THREE.Vector2(0, 5/7), new THREE.Vector2(1, 5/7), new THREE.Vector2(1, 6/7), new THREE.Vector2(0, 6/7) ],
			[ new THREE.Vector2(0, 4/7), new THREE.Vector2(1, 4/7), new THREE.Vector2(1, 5/7), new THREE.Vector2(0, 5/7) ],
			[ new THREE.Vector2(0, 3/7), new THREE.Vector2(1, 3/7), new THREE.Vector2(1, 4/7), new THREE.Vector2(0, 4/7) ],
			[ new THREE.Vector2(0, 2/7), new THREE.Vector2(1, 2/7), new THREE.Vector2(1, 3/7), new THREE.Vector2(0, 3/7) ],
			[ new THREE.Vector2(0, 1/7), new THREE.Vector2(1, 1/7), new THREE.Vector2(1, 2/7), new THREE.Vector2(0, 2/7) ],
			[ new THREE.Vector2(0,   0), new THREE.Vector2(1,   0), new THREE.Vector2(1, 1/7), new THREE.Vector2(0, 1/7) ]
		]
	
		for( var i=0 ; i<geometry.faceVertexUvs[0].length ; i+=2 ){
			t = ( i%50 ) ? 0 : Math.floor(Math.random()*7)
			geometry.faceVertexUvs[0][i] = [ textures[t][0], textures[t][1], textures[t][3] ] 
			geometry.faceVertexUvs[0][i+1] = [ textures[t][1], textures[t][2], textures[t][3] ]
			
			$(geometry.faceVertexUvs[0][i]).on('mouseover',function(e){
				$('button').css('cursor','pointer')
			}) 
		} 
	
		mesh = new THREE.Mesh( geometry, material )
		scene.add( mesh )  
	})
	
	function render( t ){
	
		var timer = Date.now() * 0.00001;
	
		if( moving ){
			camera.position.x = Math.cos( timer ) * radius
			camera.position.z = - Math.sin( timer ) * radius
		}
		
		if( !moving && isUserInteracting ){
			lat = -Math.max( - 85, Math.min( 85, lat ) )
			phi = THREE.Math.degToRad( 90 - lat )
			theta = THREE.Math.degToRad( lon )
		
			camera.lookAt( new THREE.Vector3(
				500 * Math.sin( phi ) * Math.cos( theta ),
				500 * Math.cos( phi ),
				500 * Math.sin( phi ) * Math.sin( theta )
			))
		}
		else
		{
			viewX = Math.cos( timer-50 ) * radius;
			viewZ = - Math.sin( timer-50  ) * radius;
			camera.lookAt( new THREE.Vector3( viewX, 0, viewZ ) )
		}
	
		renderer.render( scene, camera )
		window.requestAnimationFrame( render )
	}
	
	render( new Date().getTime() )
}
