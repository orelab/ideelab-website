var anim_demo3d = function( dom )
{
	var scene = new THREE.Scene()
	
	var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 )
//	var camera = new THREE.OrthographicCamera( $( dom ).width()/-50, $( dom ).width()/50, $( dom ).height()/50, $( dom ).height()/-50, 0, 1000 )
	camera.position.y = 15
	camera.position.z = 25
	
	var renderer = new THREE.WebGLRenderer({ antialias:true, alpha:true })
	renderer.shadowMapEnabled = true
	renderer.shadowMapType = THREE.PCFSoftShadowMap	// antialis the shadow
	renderer.setSize( $( dom ).width(), $( dom ).height() )
	$( dom ).html( renderer.domElement )

	var clock = new THREE.Clock()
	var controls = new THREE.TrackballControls( camera, document.getElementById(dom.replace('#','')) )

	var ambientLight = new THREE.AmbientLight( 0xcccccc )
	scene.add( ambientLight )

	var spot1 = new THREE.SpotLight( 0xB3E8FF, 0.3, 0, 1 )
	spot1.position.set( 40, -10, 30 )
	spot1.castShadow = true
	spot1.shadowDarkness = 1
	spot1.shadowCameraVisible = true
	scene.add( spot1 )

	var spot2 = new THREE.SpotLight( 0xB3E8FF, 0.3, 0, 1 )
	spot2.position.set( -40, -10, 30 )
	spot2.castShadow = true
	spot2.shadowDarkness = 1
	spot2.shadowCameraVisible = true
	scene.add( spot2 )

					
	var loader = new THREE.JSONLoader()
/*
	loader.load( 'assets/3d/tardis.js', function(geo, mat)
	{
		lmat = new THREE.MeshLambertMaterial({
			map: THREE.ImageUtils.loadTexture('assets/3d/tardisTextureMap.png')
		})
		tardis = new THREE.Mesh( geo, lmat )
		tardis.position.z = -5
		tardis.position.y = 10
		tardis.rotation.z -= 0.15
		scene.add( tardis )
	}, 'assets/3d/')

	loader.load( 'assets/3d/bruno.js', function(geo, mat)
	{
		bruno = new THREE.Mesh( geo, new THREE.MeshFaceMaterial(mat) )
		bruno.position.x = -15
		scene.add( bruno )

	}, 'assets/3d/')
*/
	loader.load( '/3d/bruno.js', function(geo, mat)
	{
		lmat = new THREE.MeshFaceMaterial(mat)
		lmat = new THREE.MeshPhongMaterial({
			envMap: THREE.ImageUtils.loadTexture( '/3d/bg.png', THREE.SphericalReflectionMapping ),
			map: THREE.ImageUtils.loadTexture('/3d/bruno.jpg')
		})

		bruno = new THREE.Mesh( geo, lmat )
		bruno.position.x = -15
		bruno.rotation.y += 0.8
		bruno.castShadow = true
		bruno.receiveShadow = false
		scene.add( bruno )

		spot2.target = bruno
	}, '/3d/')

	loader.load( '/3d/aurelien.js', function(geo, mat)
	{
		lmat = new THREE.MeshPhongMaterial({
			envMap: THREE.ImageUtils.loadTexture( '/3d/bg.png', THREE.SphericalReflectionMapping ),
			map: THREE.ImageUtils.loadTexture('/3d/aurelien.jpg')
		})

		aurelien = new THREE.Mesh( geo, lmat )
		aurelien.position.x = 15
		aurelien.rotation.y -= 0.8
		aurelien.castShadow = true
		aurelien.receiveShadow = false
		scene.add( aurelien )

		spot1.target = aurelien
	}, 'assets/3d/')

	loader.load( '/3d/idee-lab.js', function(geo, mat)
	{
		lmat = new THREE.MeshFaceMaterial(mat)
		lmat = new THREE.MeshPhongMaterial({
			envMap: THREE.ImageUtils.loadTexture( '/3d/bg.png', THREE.SphericalReflectionMapping )
		})
		ideelab = new THREE.Mesh( geo, lmat )
		ideelab.castShadow = true
		ideelab.receiveShadow = false

		scene.add( ideelab )
	}, '/3d/')

	var logo_rotation = function( delta )
	{
		step = delta / 10
		if( typeof ideelab != 'undefined' )
		{
			ideelab.rotation.y += step
		}
	}
/*
	var turnRight = false
	
	var team_rotation = function( delta )
	{
		step = delta / 100

		if( bruno.rotation.z > 0.2 ) turnRight = false
		if( bruno.rotation.z < -0.2 ) turnRight = true

		if( turnRight )
		{
			bruno.rotation.z += step
			aurelien.rotation.z -= step
		}
		else
		{
			bruno.rotation.z -= step
			aurelien.rotation.z += step
		}
	}

	var tardisRight = false

	var tardis_rotation = function( delta )
	{
		step = delta * 10

		if( tardis.position.x > 50 ) tardisRight = false
		if( tardis.position.x < -50 ) tardisRight = true
		
		if( tardisRight )
			tardis.position.x += step
			else
			tardis.position.x -= step
			
		tardis.rotation.y += 0.25
	}
*/	
	var rendering = function ()
	{
		var delta = clock.getDelta()
		
		requestAnimationFrame( rendering )
		controls.update( delta )

		logo_rotation( delta )
//		team_rotation( delta )
//		tardis_rotation( delta )

		renderer.render(scene, camera)
	}
	rendering()
}






