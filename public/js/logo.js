anim_logo = function( dom )
{
	var w = $( dom ).width()
	var h = w / 4 * 3 - $('nav').height()

	var scene = new THREE.Scene()
	
	var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 )
	//var camera = new THREE.OrthographicCamera( w/-50, w/50, h/50, h/-50, 0, 1000 )
	camera.position.y = 0
	camera.position.z = 15
	
	var renderer = new THREE.WebGLRenderer({ antialias:true, alpha:true })
	renderer.shadowMapEnabled = true
	renderer.shadowMapType = THREE.PCFSoftShadowMap	// antialias the shadow
	renderer.setSize( w, h )
	$( dom ).html( renderer.domElement )
	
	$(window).on('resize',function(){
		camera.aspect = $( dom ).width() / ($( dom ).height()-100)
		camera.updateProjectionMatrix()

		renderer.setSize( $( dom ).width(), $( dom ).height()-100 )
	})
	$(window).resize()

	var clock = new THREE.Clock()
	var controls = new THREE.TrackballControls( camera, document.getElementById(dom.replace('#','')) )

	var ambientLight = new THREE.AmbientLight( 0xcccccc )
	scene.add( ambientLight )

	var spot1 = new THREE.SpotLight( 0xB3E8FF, 0.3, 0, 1 )
	spot1.position.set( 40, -10, 30 )
	spot1.castShadow = true
	spot1.shadowDarkness = 1
	//spot1.shadowCameraVisible = true
	scene.add( spot1 )

	var spot2 = new THREE.SpotLight( 0xB3E8FF, 0.3, 0, 1 )
	spot2.position.set( -40, -10, 30 )
	spot2.castShadow = true
	spot2.shadowDarkness = 1
	//spot2.shadowCameraVisible = true
	scene.add( spot2 )

	var turnRight = true
	
	var rotation_logo = function( delta )
	{
	//	if( ideelab.rotation.y > 0.2 ) turnRight = false
	//	if( ideelab.rotation.y < -0.2 ) turnRight = true

		step = delta / 10
		
		if( turnRight )
			ideelab.rotation.y += step
			else
			ideelab.rotation.y -= step
	}

	var render = function ()
	{
		var delta = clock.getDelta()
		
		requestAnimationFrame( render )

		if( /* active_logo */ true )
		{
			controls.update( delta )
			rotation_logo( delta )
			renderer.render(scene, camera)
		}
	}

					
	var loader = new THREE.JSONLoader()

	loader.load( '/3d/idee-lab.js', function(geo, mat)
	{
		lmat = new THREE.MeshFaceMaterial(mat)
		lmat = new THREE.MeshPhongMaterial({
			envMap: THREE.ImageUtils.loadTexture( '/3d/bg.png', THREE.SphericalReflectionMapping )
		})
		ideelab = new THREE.Mesh( geo, lmat )
		ideelab.castShadow = true
		ideelab.receiveShadow = false
		scene.add( ideelab )
		
		render()
	}, '/3d/')
	
}

$('body').css('background-image', 'none')
anim_logo( '#logo' )
//$('#logo').prepend('<p>mode 3D</p>' )

