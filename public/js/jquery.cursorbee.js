(function($){


	$.fn.cursorbee = function( options )
	{
		var opt = $.extend({
		}, options);


		var me = $(this);
		
		var lastMove = (new Date()).getTime();
		
		var dir = 'left';
		
		var pos = '0';
		
		var prevX = 0;
		
		var prevY = 0;


		var moving = function( e )
		{
			lastMove = (new Date()).getTime();

			if( prevX < e.pageX )
			{
				dir = 'right';
			}
			else if( prevX > e.pageX )
			{
				dir = 'left';
			}
			
			pos =  lastMove%3;
			
			me.css('cursor', 'url(/img/cursor-bee-' + dir + '-' + pos + '.png) 15 15, auto' );
			prevX = e.pageX;
		}

		
		freeze = function()
		{
			if( (new Date()).getTime() - lastMove > 10  )
			{
				me.css('cursor', 'url(/img/cursor-bee-' + dir + '-stop.png) 15 15, auto' );
			}
		}

		var timer = setInterval( "freeze()", 100 );

		return this.each(function()
		{
			$(this).on( 'mousemove', moving );
		});


	}

}(jQuery));


