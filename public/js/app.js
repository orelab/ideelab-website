


/** *************************************************************
		
							IDEELAB constructor
		
		
				IDEELAB.dovisit()
				(external loading)
						||
						\/
			IDEELAB.doLoadUrl()		IDEELAB.keyboardControl()
			(when click on a <a>)	(on click escape)
								||			||
								\/			\/
								IDEELAB.load()
								/\			/\
								||			||
			window.onpopstate			window.location.hash
			(roll back history)		(non-ajax page loading)
		
 ************************************************************** */



// security is used to prevent numerous IDEELAB loading

security = 0;



var IDEELAB = function()
{
	
	if( security++ ) return;
	
	$('nav a').on('click', IDEELAB.doLoadUrl);
	$('body').delegate('.tableau a', 'click', IDEELAB.doLoadUrl);
	$('body').delegate('#secondary>img', 'click', IDEELAB.close);
	$(document).keyup( IDEELAB.keyboardControl );
}


IDEELAB.currentUrl = '';

IDEELAB.method = 'DIAPO'; // method = DIAPO or POST

IDEELAB.duration = 700;

IDEELAB.effect = 'easeOutBounce';

IDEELAB.effect = 'easeOutQuart';


IDEELAB.doLoadUrl = function(e)
{
	if( $(this).hasClass('noajax') ) return;

	e.preventDefault();

	var slug = $(this).attr('href');

	IDEELAB.method = $(this).parents().is('nav') ? "DIAPO" : "POST";
	
	if( slug != IDEELAB.currentUrl )
		IDEELAB.load( slug );
		else
		IDEELAB.close();
}


IDEELAB.load = function( slug )
{
	if( ! slug )
	{
		return false;
	}

	IDEELAB.registerUrl( slug );

	$('nav a').removeClass('hover');
	$('nav a[href="'+slug+'"]').toggleClass('hover');
	
	document.title = 'Idée Lab - ' + IDEELAB.formatTitle( slug );

	if( typeof unload === 'function' && IDEELAB.method == 'DIAPO' )
	{
		unload(); 
	}
	
	if( IDEELAB.method == 'DIAPO' )
	{
		$('section').each(function(){
			var me = $(this);
			setTimeout(function(){me.remove()}, IDEELAB.duration);
		});
	}
	
	$.post( slug+'.html', {action:'diapositive',slug:slug}, function(data)
	{
		if( ! $(data).is('section') )
		{
			console.log('Cette page est vide !');
		}
		
		var page = $(data).appendTo('body');
		
		if( typeof pre_load === 'function' && IDEELAB.method == 'DIAPO' )
		{
			setTimeout(pre_load, 0);	// launch pre_load() in a new thread
		}

		page.animate({top:0},{
			duration: IDEELAB.duration,
			easing: IDEELAB.effect,
			complete: function(){
				$(this).css('overflow','auto')
				
				if( typeof post_load === 'function' && IDEELAB.method == 'DIAPO' )
				{
					setTimeout(post_load, 0);	// launch pre_load() in a new thread
				}
			}
		});
	});
	IDEELAB.currentUrl = slug;
}





IDEELAB.close = function()
{
	if( typeof unload === 'function' )
	{
		setTimeout(unload, 0);	// launch unload() in a new thread
	}

	//-- close page

	document.title = 'Idée Lab';

	//-- close the current section
	
	var section = $(this).parents().filter('section');

	//-- if no current section, close all existing sections
	 	
	if( ! section.length )
	{
		section = $('section');
	}
	
	$(section).stop().animate({top:'100%'},{
		duration: IDEELAB.duration,
		easing: IDEELAB.effect,
		complete: function(){ $(this).remove() }
	});
	IDEELAB.currentUrl = null;

	if( IDEELAB.method == 'POST' )
	{
		IDEELAB.method = 'DIAPO';
	//	history.back();
	}	
}





IDEELAB.formatTitle =  function( slug )
{
	return slug.replace('-',' ').replace('.php','');
}





IDEELAB.registerUrl = function( slug )
{
	console.log( slug );
	history.pushState( slug, IDEELAB.formatTitle(slug), '#'+slug );
}




	
IDEELAB.dovisit = function( slug )
{
	IDEELAB.doLoadUrl( slug );
	return false;
}





IDEELAB.keyboardControl = function(e)
{
	if( e.keyCode==27 )
	{
		if( $('#secondary img').length ) 
			$('#secondary img').click();
			else
			IDEELAB.load( IDEELAB.currentUrl );
	}
}



$('document').ready(function()
{
	
	//-- 3D logo

	Modernizr.load({
		test: Modernizr.webgl,
		yep : [
			'/js/three.js',
			'/js/TrackballControls.js',
//			'/js/logo.js',
			'/js/demo3d.js',
			'/js/grotte.js'
		]
	});
	
	window.onpopstate = function(e)
	{
		if( ! e.state ) return;
		IDEELAB.load( e.state, "DIAPO" );
	}
	
	
	if( window.location.hash )
	{
		var slug = window.location.hash.substring(1);
		IDEELAB.load( slug, "DIAPO" );
	}
	
	if( window.location.pathname )
	{
	//	var slug = window.location.pathname.replace(new RegExp(/\//, 'g'), '');
		var slug = window.location.pathname.replace(/\//g, '');
	//	window.location.href = '/#' + slug;
	}
	
	var il = new IDEELAB();
	
	$('html').cursorbee();
	
});









