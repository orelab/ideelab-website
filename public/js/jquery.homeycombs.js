(function($) {

    $.fn.honeycombs = function(options)
    {

        var settings = $.extend({
            combWidth: 250,
            margin: 5,
            hiddenTitle: false
        }, options);

        function initialise(element) {

            // brassage aléatoire
            $(element).children()
            	.detach()
            	.sort(function(a,b){return Math.floor(Math.random()*2)})
            	.sort(function(a,b){return Math.floor(Math.random()*2)})
            	.appendTo(element);
            
            $(element).addClass('honeycombs-wrapper');
            
            var width = 0;
            var combWidth = 0;
            var combHeight = 0;
            var num = 0;
            var $wrapper = null;
            
            /**
             * Build the dom
             */
            function buildHtml(){
                // add the 2 other boxes
                $(element).find('.comb').wrapAll('<div class="honeycombs-inner-wrapper"></div>');
                $wrapper = $(element).find('.honeycombs-inner-wrapper');
                
                $(element).find('.comb').append('<div class="hex_l"></div>');
                $(element).find('.hex_l').append('<div class="hex_r"></div>');
                $(element).find('.hex_r').append('<div class="hex_inner"></div>');

                $(element).find('.hex_inner').append('<div class="inner_span"><div class="inner-text"></div></div>');
                
                num = 0;
                
                $(element).find('.comb').each(function(){
                    num = num + 1;
                    var image = $(this).find('img').attr('src');
                    var css = 'url("'+image+'") ';
                    
                    $(this).find('.inner_span').attr('style', 'background-image: '+css);
                    
                    if($(this).find('span').length > 0){
                        $(this).find('.inner_span .inner-text').html($(this).find('span').html());
                    } else {
                        $(this).find('.inner_span .inner-text').remove();
                    };
                });
                
                $(element).find('img, span, .inner_span ul').hide();
            }
            
            /**
             * Update all scale values
             */
            function updateScales(){
                combHeight = settings.combWidth;
                combWidth = ( Math.sqrt(3) * settings.combWidth ) / 2;

                edgeWidth = combWidth / 2;
                
                
                $(element).find('.comb').width(combWidth).height(combHeight);
                $(element).find('.hex_l, .hex_r').width(combWidth).height(combHeight);
                $(element).find('.hex_inner').width(combWidth).height(combHeight);
            }
            
            /**
             * update css classes
             */
            function reorder(animate){
                
                updateScales();
                width = $(element).width();
                
                newWidth = ( num / 1.5) * settings.combWidth;
                
                if(newWidth < width){
                    width = newWidth;
                }

                $wrapper.width(width);
                
                var row = 0; // current row
                var left = 0; // pos left
                var top = 0; // pos top
                
                var cols = 0;
                
                $(element).find('.comb').each(function(index){

                    top = row * (combHeight * 3 / 4 + settings.margin);
                    
                    if(animate == true){
                        $(this).stop(true, false);
                        $(this).animate({'left': left, 'top': top},800);
                    }else{
                        $(this).css('left', left).css('top', top);
                    }
                    
                    left = left + combWidth + settings.margin;
                    
                    if(row == 0){
                        cols = cols + 1;
                    }
                        
                    if(left + combWidth > width-60){	// 60 for element padding
                        left = row%2 ? 0 :edgeWidth + settings.margin/2;
                        row = row + 1;
                    }
                });
                
//                $wrapper
//                    .width(cols * (combHeight / 4 * 3 + settings.margin) + combHeight / 4)
//                    .height((row + 1) * (combHeight + settings.margin) + combHeight / 2);
            }
            
            $(window).resize(function(){
                reorder(true);
            });


				if( settings.hiddenTitle )
				{
	            $(element).find('.comb').mouseenter(function(){
	                $(this).find('.inner-text').stop(true, true).fadeIn();
	            });
	            
	            $(element).find('.comb').mouseleave(function(){
	                $(this).find('.inner-text').stop(true, true).fadeOut();
	            });
				}
				else
				{
	            $(element).find('.comb').mouseenter(function(){
	                $(this).find('.inner-text').stop(true, true).animate({'padding-top':'10%'});
	                $(this).find('.inner_span ul').stop(true, true).fadeIn();
	            });
	            
	            $(element).find('.comb').mouseleave(function(){
	                $(this).find('.inner-text').stop(true, true).animate({'padding-top':'30%'});
	                $(this).find('.inner_span ul').stop(true, true).fadeOut();
	            });
         	}
            
            buildHtml();
            reorder(true);
        }
        
        return this.each(function() {
            initialise(this);
        });

    }

}(jQuery));